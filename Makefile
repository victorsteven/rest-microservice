build:
#	protoc -I. --go_out=plugins=grpc:. \
#	api/proto/v1/todo-service.proto

	protoc -I./third_party  --proto_path=api/proto/v1 --go_out=plugins=grpc:api/proto/v1 todo-service.proto
	protoc -I./third_party  --proto_path=api/proto/v1 --grpc-gateway_out=logtostderr=true:api/proto/v1 todo-service.proto
	protoc -I./third_party  --proto_path=api/proto/v1 --swagger_out=logtostderr=true:api/swagger/v1 todo-service.proto

#	protoc -I./third_party
#	protoc --proto_path=api/proto/v1 --proto_path=third_party --go_out=plugins=grpc:pkg/api/v1 todo-service.proto
#	protoc --proto_path=api/proto/v1 --proto_path=third_party --grpc-gateway_out=logtostderr=true:pkg/api/v1 todo-service.proto
#	protoc --proto_path=api/proto/v1 --proto_path=third_party --swagger_out=logtostderr=true:api/swagger/v1 todo-service.proto

proto:
	protoc -I/third_party -I. \
	-I${GOPATH}/src \
	-I/third_party/google/api/annotations \
	-I/third_party/google/api/annotations \
	--go_out=plugins=grpc:. \
	pb/hello.proto

proto-link:
	./protobuf-import.sh
	protoc --proto_path=protobuf-import  --gofast_out=plugins=grpc:./third_party todo-service.proto