package main

import (
	"fmt"
	"os"
	"rest-microservice/pkg/cmd"
)

func main(){
	if err := cmd.RunServer(); err != nil {
		fmt.Fprintf(os.Stderr, "the error: %v\n", err)
		os.Exit(1)
	}
}
