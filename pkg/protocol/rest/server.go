package rest

import (
	"context"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc"
	"log"
	"net/http"
	"os"
	"os/signal"
	v1 "rest-microservice/api/proto/v1"
	"time"
)

//RunServer runs HTTP/REST gateway
func RunServer(ctx context.Context, grpcPort, httpPort string) error {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	if err := v1.RegisterToDoServiceHandlerFromEndpoint(ctx, mux, "localhost:"+grpcPort, opts);  err != nil {
		log.Fatalf("failed to start HTTP gateway: %v", err)
	}
	srv := &http.Server{
		Addr: ":"+httpPort,
		Handler: mux,
	}

	//graceful shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for range c {

		}
		_, cancel := context.WithTimeout(ctx, 5 * time.Second)
		defer cancel()

		_ = srv.Shutdown(ctx)
	}()

	log.Println("Starting HTTP/REST gateway...")
	return srv.ListenAndServe()
}