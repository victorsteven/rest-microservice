package cmd

import (
	"context"
	"database/sql"
	"log"
	"os"
	//"flag"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
	"rest-microservice/pkg/protocol/grpc"
	"rest-microservice/pkg/protocol/rest"
	v1 "rest-microservice/pkg/service/v1"
)

func init() {
	//loads values from .env into the system
	if err := godotenv.Load(os.ExpandEnv("./../../.env")); err != nil {
		log.Print("sad .env file found")
	}
}

type Config struct {
	//gRPC server start parameters section
	//gRPC is TCP port to listen by gRPC server
	GRPCPort string
	HTTPPort string
	DatastoreDBHost string
	DatastoreDBUser string
	DatastoreDBPassword string
	DatastoreDBSchema string
	DatastoreDBPort string
}

//RunServer runs gRPC server and HTTP gateway

func RunServer() error {
	ctx := context.Background()

//	get configuration
//	var cfg Config
//	flag.StringVar(&cfg.GRPCPort, "grpc-port", "", "gRPC port to bind")
//	flag.StringVar(&cfg.DatastoreDBHost, "db-host", "", "Database host")
//	flag.StringVar(&cfg.DatastoreDBUser, "db-user", "", "Database user")
//	flag.StringVar(&cfg.DatastoreDBPassword, "db-password", "", "Database password")
//	flag.StringVar(&cfg.DatastoreDBSchema, "db-schema", "", "Database schema")
//	flag.Parse()
//	if len(cfg.GRPCPort) == 0 {
//		return fmt.Errorf("invalid TCP port for gRPC server: '%s'", cfg.GRPCPort)
//	}

	var cfg Config
	cfg.DatastoreDBSchema = os.Getenv("DatastoreDBSchema")
	cfg.DatastoreDBHost = os.Getenv("DatastoreDBHost")
	cfg.DatastoreDBPort = os.Getenv("DatastoreDBPort")
	cfg.DatastoreDBUser = os.Getenv("DatastoreDBUser")
	cfg.DatastoreDBPassword = os.Getenv("DatastoreDBPassword")
	cfg.GRPCPort = os.Getenv("GRPCPort")
	cfg.HTTPPort = os.Getenv("HTTPPort")


	//	Add MYSQL driver specific parameter to parse date/time
	//Drop it fo another database
	//param := "parseTime=true"

	if (len(cfg.GRPCPort) == 0) {
		return fmt.Errorf("invalid TCP port for gRPC server: '%s'", cfg.GRPCPort)
	}

	if len(cfg.HTTPPort) == 0 {
		return fmt.Errorf("invalid TCP port for HTTP gateway: '%s'", cfg.HTTPPort)
	}

	dns := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		cfg.DatastoreDBUser,
		cfg.DatastoreDBPassword,
		cfg.DatastoreDBHost,
		cfg.DatastoreDBPort,
		cfg.DatastoreDBSchema,
	)

	db, err := sql.Open("mysql", dns)
	if err != nil {
		return fmt.Errorf("failed to open database: %v", err)
	}
	if pingErr  := db.Ping(); pingErr != nil {
		fmt.Println("this is the ping error: ", pingErr)
	} else {
		fmt.Println("We pinged successfully")
	}

	defer db.Close()

	v1API := v1.NewToDoServiceServer(db)

	//run HTTP gateway
	go func() {
		_ = rest.RunServer(ctx, cfg.GRPCPort, cfg.HTTPPort)
	}()
	return grpc.RunServer(ctx, v1API, cfg.GRPCPort)
}